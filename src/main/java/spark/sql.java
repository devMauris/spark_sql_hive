/**
 * Created by mauris on 10/11/15.
 */
package spark;
import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.hive.*;
import java.util.*;


public class sql
{
    public static void main(String[] args)
    {
        try
        {
            SparkConf sparkConf = new SparkConf().setAppName("JavaSparkSQL");
            SparkContext sc = new SparkContext(sparkConf);
            HiveContext sqlContext = new HiveContext(sc);

            String[] queries = args[0].split(";");

            for(String q : queries)
            {
                System.out.println(q);
                sqlContext.sql(q);
                //Row[] rows = sqlContext.sql(q).collect();
              //  for (Row r : rows)
               //     System.out.println(r.toString());
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
            System.exit(1);
        }
    }
}
